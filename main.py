#
# def fib_n(n) :
#     if n == 1:
#         return 0
#     elif n == 2 :
#         return 1
#     else :
#         n1 = 0
#         n2 = 1
#         count = 3
#         while count <= n:
#             temp = n2
#             n2 = n1 + n2
#             n1 = temp
#             count += 1
#         return n2
# n = int(input('Please enter a number : '))
# print(fib_n(n))

# Using recursive methods
def fib_of_n(number):
    if number == 1 :
        return 0
    elif number == 2 :
        return 1
    else:
        return (fib_of_n(number-1))+(fib_of_n(number-2))

n = int(input('Please enter a number : '))
print(fib_of_n(n))
